
var input = '';
var operation = '';
var aux = 0;

function printVariables() {
    console.log(input);
    console.log(aux);
    console.log(result.innerText);
    console.log(operation);
}

function checkOperators(character) {
    return (character !== '-') && (character !== '+') && (character !== '*') &&
      (character !== '/') && (character !== '=');
}

function displayInput(character) {
    input = `${input}${character}`;
    result.innerText = input;
}

function applyOperators(operation, aux, input) {
    if (operation === '+') {
        return aux + input;
    }
    if (operation === '-') {
        return aux - input;
    }
    if (operation === '*') {
        return aux * input;
    }
    if (operation === '/') {
        return aux / input;
    }
}

function clickedButton(character) {
    var result = document.getElementById('result');
    if (checkOperators(character)) {
        displayInput(character);
    } else {
        document.getElementById('operation').innerText = character;
        if (aux === 0) {
            if (input !== '') {
                [aux, input] = [parseFloat(input), ''];
            } else {
                aux = result.innerText;
            }
        } else {
            if (input !== '') {
                if (operation === "=") {
                    if (aux !== result.innerText) {
                        [aux, input] = [input, ''];
                    } else {
                        result.innerText = applyOperators(character, parseFloat(aux), parseFloat(input));
                        [aux, input] = [`${parseFloat(result.innerText)}`, ''];}
                } else {
                    result.innerText = applyOperators(operation, parseFloat(aux), parseFloat(input));
                    [aux, input] = [`${parseFloat(result.innerText)}`, ''];
                }
            }
        }
        operation = character;
    }
}
